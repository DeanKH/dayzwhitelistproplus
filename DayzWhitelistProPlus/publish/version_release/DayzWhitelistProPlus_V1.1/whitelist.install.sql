-- --------------------------------------------------------
-- Host:                         85.234.146.76
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4218
-- Date/time:                    2012-11-07 19:58:41
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for whitelist
CREATE DATABASE IF NOT EXISTS `whitelist` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `whitelist`;


-- Dumping structure for table whitelist.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `GUID` varchar(32) NOT NULL,
  `timestamp` datetime NOT NULL,
  `logtype` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table whitelist.logtypes
CREATE TABLE IF NOT EXISTS `logtypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `logtypes` (`id`, `description`) VALUES (1, 'Authorised');
INSERT INTO `logtypes` (`id`, `description`) VALUES (2, 'Declined');


-- Dumping structure for procedure whitelist.proc_AddWhiteListed
DELIMITER //
CREATE DEFINER=`dayz`@`%` PROCEDURE `proc_AddWhiteListed`(IN `p_name` vARCHAR(255), IN `p_email` vARCHAR(255), IN `p_GUID` vARCHAR(128))
BEGIN
  INSERT IGNORE INTO whitelist (`identifier`, `email`, `name`) VALUES (p_GUID, p_email, p_name);
END//
DELIMITER ;


-- Dumping structure for procedure whitelist.proc_CheckWhiteList
DELIMITER //
CREATE DEFINER=`dayz`@`%` PROCEDURE `proc_CheckWhiteList`(IN `p_guid` VARCHAR(32))
BEGIN
  SELECT *
  FROM
    whitelist
  WHERE
    whitelist.identifier = p_guid
    AND whitelist.whitelisted = 1;
END//
DELIMITER ;


-- Dumping structure for procedure whitelist.proc_GetWhitelisted
DELIMITER //
CREATE DEFINER=`dayz`@`%` PROCEDURE `proc_GetWhitelisted`()
BEGIN
  SELECT *
  FROM
    whitelist.whitelist;
END//
DELIMITER ;


-- Dumping structure for procedure whitelist.proc_GetWhitelistLog
DELIMITER //
CREATE DEFINER=`dayz`@`%` PROCEDURE `proc_GetWhitelistLog`()
BEGIN
  SELECT log.id
       , log.name
       , log.GUID
       , log.`timestamp`
       , logtypes.description AS type

  FROM
    whitelist.log
  INNER JOIN whitelist.logtypes
  ON log.logtype = logtypes.id

  GROUP BY
    GUID
  ORDER BY
    log.id DESC
  ;
END//
DELIMITER ;


-- Dumping structure for procedure whitelist.proc_LogWhiteList
DELIMITER //
CREATE DEFINER=`dayz`@`%` PROCEDURE `proc_LogWhiteList`(IN `p_name` VARCHAR(255), IN `p_GUID` VARCHAR(128), IN `p_logtype` INT)
BEGIN
  INSERT INTO whitelist.log (name, GUID, `timestamp`, logtype) VALUES (p_name, p_GUID, now(), p_logtype);
END//
DELIMITER ;


-- Dumping structure for procedure whitelist.proc_SetWhitelistedStatus
DELIMITER //
CREATE DEFINER=`dayz`@`%` PROCEDURE `proc_SetWhitelistedStatus`(IN `p_GUID` VARCHAR(128), IN `p_whitelisted` INT)
BEGIN
  UPDATE whitelist
  SET
    whitelisted = p_whitelisted
  WHERE
    whitelist.identifier = p_GUID;
END//
DELIMITER ;


-- Dumping structure for table whitelist.whitelist
CREATE TABLE IF NOT EXISTS `whitelist` (
  `identifier` varchar(255) NOT NULL COMMENT 'guid or IP',
  `email` varchar(255) DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `whitelisted` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
