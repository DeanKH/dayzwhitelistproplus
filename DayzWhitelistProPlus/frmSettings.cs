﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayzWhitelistProPlus
{
    public partial class frmSettings : Form
    {
        private frmMain parentForm;

        public frmSettings(frmMain form1)
        {
            InitializeComponent();
            parentForm = form1;

            // DB settings
            txtSettingDbDatabase.Text = Properties.Settings.Default.dbDatabase;
            txtSettingDbIP.Text       = Properties.Settings.Default.dbHost;
            txtSettingDbPassword.Text = Properties.Settings.Default.dbPass;
            txtSettingDbUsername.Text = Properties.Settings.Default.dbUser;
            txtSettingDbPort.Text     = Properties.Settings.Default.dbPort.ToString();

            // Rcon settings
            txtSettingRconIP.Text       = Properties.Settings.Default.rconIP;
            txtSettingRconPort.Text     = Properties.Settings.Default.rconPort.ToString();
            txtSettingRconPassword.Text = Properties.Settings.Default.rconPass;

            txtWelcomeMessage.Text = Properties.Settings.Default.welcomeMessage;
            txtKickMessage.Text = Properties.Settings.Default.kickMessage;
            txtUsernameMessage.Text = Properties.Settings.Default.usernameMessage;

            chkEnableDynamicWhitelist.Checked = (Properties.Settings.Default.dynamicWhitelistEnabled) ? true : false;
            numOnWhenBelow.Value = Properties.Settings.Default.enableWhitelistBelow;
            numOffWhenAbove.Value = Properties.Settings.Default.disableWhitelistAbove;

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            // Dynamic whitelist settings
            if (chkEnableDynamicWhitelist.Checked)
                Properties.Settings.Default.dynamicWhitelistEnabled = true;
            else
                Properties.Settings.Default.dynamicWhitelistEnabled = false;

            Properties.Settings.Default.disableWhitelistAbove = (int) numOffWhenAbove.Value;
            Properties.Settings.Default.enableWhitelistBelow = (int) numOnWhenBelow.Value;

            // DB settings
            Properties.Settings.Default.dbDatabase = txtSettingDbDatabase.Text;
            Properties.Settings.Default.dbHost = txtSettingDbIP.Text;
            Properties.Settings.Default.dbPass = txtSettingDbPassword.Text;
            Properties.Settings.Default.dbUser = txtSettingDbUsername.Text;
            Properties.Settings.Default.dbPort = Convert.ToInt32(txtSettingDbPort.Text);

            // Rcon settings
            Properties.Settings.Default.rconIP = txtSettingRconIP.Text;
            Properties.Settings.Default.rconPort = Convert.ToInt32(txtSettingRconPort.Text);
            Properties.Settings.Default.rconPass = txtSettingRconPassword.Text;

            // Misc
            Properties.Settings.Default.welcomeMessage = txtWelcomeMessage.Text;
            Properties.Settings.Default.kickMessage = txtKickMessage.Text;
            Properties.Settings.Default.usernameMessage = txtUsernameMessage.Text;

            Properties.Settings.Default.Save();

            parentForm.updateSettings();

            this.Close();
        }

        private void chkEnableDynamicWhitelist_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEnableDynamicWhitelist.Checked)
            {
                numOnWhenBelow.Enabled = true;
                numOffWhenAbove.Enabled = true;
                label15.Enabled = true;
                label17.Enabled = true;
            }
            else
            {
                numOnWhenBelow.Enabled = false;
                numOffWhenAbove.Enabled = false;
                label15.Enabled = false;
                label17.Enabled = false;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
