﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using BattleNET;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using System.Timers;

namespace DayzWhitelistProPlus
{
    
    /*  THINGS TO ADD TO CONSOLE VERSION:
     * 
     *  - USERNAME CHECK
     *  - GLOBAL CHAT COMMAND RECOGNITION - DONE
     *  - RECONNECTION FIXES
     *  
     *  OTHER THINGS TO DO:
     *  
     *  - Combine the whitelister + rcon client into one solution.
     * 
     * */

    public partial class frmMain : Form
    {
        // Load rcon credentials
        public static string ip       = Properties.Settings.Default.rconIP;
        public static int port        = Properties.Settings.Default.rconPort;
        public static string password = Properties.Settings.Default.rconPass;

        // Load database credentials
        public static String dbHost     = Properties.Settings.Default.dbHost;
        public static String dbUser     = Properties.Settings.Default.dbUser;
        public static String dbPort     = Properties.Settings.Default.dbPort.ToString();
        public static String dbDatabase = Properties.Settings.Default.dbDatabase;
        public static String dbPassword = Properties.Settings.Default.dbPass;

        static IBattleNET b;

        public System.Timers.Timer keepAliveTimer;

        //private Timer keepAliveTimer;
        private System.Windows.Forms.Timer reconnectTimer;
        Color bgcolor;

        private static bool isConnected = false;

        /* Options: need loading from config file eventually */
        private static bool showChat = true;
        private static bool showWelcomeMsg = true;
        private static bool showStartupMsg = true;
        private static bool showDynamicWhitelistMsg = true;
        private static bool whiteListEnabled = true;
        private static bool addNewPlayersWhenDisabled = false;
        private static bool mustMatchUsername = false;
        private static bool dynamicWhitelistEnabled = Properties.Settings.Default.dynamicWhitelistEnabled;

        private static int enableWhitelistBelow = Properties.Settings.Default.enableWhitelistBelow;
        private static int disableWhitelistAbove = Properties.Settings.Default.disableWhitelistAbove;

        public static String welcomeMessage = Properties.Settings.Default.welcomeMessage;
        public static String kickMessage = Properties.Settings.Default.kickMessage;
        public static String kickMessageWrongUsername = Properties.Settings.Default.usernameMessage;
        
        delegate void DisconnectCallback(string text);
        delegate void DumpMessageCallback(string text);
        
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.firstTimeOpening)
            {
                frmSettings frmS = new frmSettings(this);
                frmS.ShowDialog();

                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.firstTimeOpening = false;
                Properties.Settings.Default.Save();
            }
        }

        /* DISCONNECT EVENT */
        private void Disconnected(BattlEyeDisconnectEventArgs args)
        {
            doDisconnect(args.Message);
        }

        private void doDisconnect(String msg)
        {
            if (this.rtbDisplay.InvokeRequired)
            {
                DisconnectCallback d = new DisconnectCallback(doDisconnect);
                this.Invoke(d, new object[] { msg + "\n" });
            }
            else
            {
                isConnected = false;
                keepAliveTimer.Start();
                this.mnuConnect.Enabled = true;
                this.rtbDisplay.AppendText(msg + "\n", Color.Gray);
                if(reconnectTimer == null || !reconnectTimer.Enabled)
                    startReconnect();
            }
        }

        /* DUMP MESSAGE EVENT */
        private void DumpMessage(BattlEyeMessageEventArgs args)
        {
            doDumpMessage(args.Message);
        }

        private void doDumpMessage(String msg)
        {   
            if (msg.Substring(0, 10) == "Players on")
            {
                //kickNonWhitelistPlayers(msg);

                if (dynamicWhitelistEnabled)
                {
                    int playerCount = checkDynamicWhitelist(msg);
                    if (playerCount < enableWhitelistBelow && whiteListEnabled == true)
                    {
                        whiteListEnabled = false;
                        if (showDynamicWhitelistMsg)
                            b.SendCommandPacket("say -1 WHITELIST: Auto-disabled");
                    }
                    else if(playerCount > disableWhitelistAbove && whiteListEnabled == false)
                    {
                        whiteListEnabled = true;
                        if(showDynamicWhitelistMsg)
                            b.SendCommandPacket("say -1 WHITELIST: Auto-enabled");
                    }
                }

                return;
            }

            setTextColor(msg);

            if (this.rtbDisplay.InvokeRequired)
            {
                DumpMessageCallback d = new DumpMessageCallback(doDumpMessage);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                // Show chat messages if enabled
                if (showChat == true)
                {
                    this.rtbDisplay.AppendText(msg, bgcolor);
                }

                // Main logic for determining when a player joins + checking whitelist status
                try
                {
                    Match matchString;

                    // Grab the user data if it matches our regular expresion - Thanks to mmmmk for this Regex!
                    matchString = Regex.Match(msg, @"Player #(?<player_id>[0-9]{1,3})\s(?<user>.+) - GUID: (?<guid>.+)\W\D\S", RegexOptions.IgnoreCase);
                    if (matchString.Success)
                    {
                        // new client obj
                        DayzClient client = new DayzClient();

                        client.GUID = matchString.Groups["guid"].Value.Trim();
                        client.playerNo = Convert.ToInt32(matchString.Groups["player_id"].Value);
                        client.UserName = matchString.Groups["user"].Value;

                        // did we get a valid result? verify
                        if (client.GUID != null && client.UserName != null)
                        {
                            if (VerifyWhiteList(client) == false)
                            {
                                // user is not white listed kick and send message
                                KickPlayer(client);
                                //addPlayer(client);

                                // log event
                                client.logType = DayzClient.LogTypes.Kick;
                                LogPlayer(client);
                            }
                            else
                            {
                                if (mustMatchUsername)
                                {
                                    if (VerifyWhitelistWithName(client) == false)
                                    {
                                        KickPlayerForWrongUsername(client);
                                    }
                                }
                                else
                                {
                                    // display welcome message
                                    WelcomeMessage(client);

                                    // log event;
                                    client.logType = DayzClient.LogTypes.Success;
                                    LogPlayer(client);
                                }
                            }
                        }

                        // destroy client obj
                        client = null;

                        //Console.WriteLine(matchString.Groups["player_id"].Value + "*" + matchString.Groups["guid"].Value + "*" + matchString.Groups["user"].Value);
                    }
                }
                catch (Exception e)
                {
                    // do nothing
                }

                // Log for determining addtional rcon commands
                try
                {
                    Match matchString;

                    // Grab the user data if it matches our regular expresion - Thanks to mmmmk for this Regex!
                    // Modified by DeanHyde to include in-game global chat
                    matchString = Regex.Match(msg, @"RCon admin # |\(Global\)(.+)(\.|#)(?<cmd>.+)\s(?<option>.+)", RegexOptions.IgnoreCase);
                    if (matchString.Success)
                    {
                        String cmd = matchString.Groups["cmd"].Value; // not used for now
                        String option = matchString.Groups["option"].Value;
                        switch (cmd)
                        {
                            // We're issuing a whitelist command
                            case "whitelist":
                                switch (option)
                                {
                                    case "on":
                                        toggleWhitelist(true, true);
                                        break;

                                    case "off":
                                        toggleWhitelist(false, true);
                                        break;
                                    // Kick all non-whitelisted players
                                    case "kick":
                                        b.SendCommandPacket(EBattlEyeCommand.Players); // Gotta be a better way of doing this than relying entirely upon the 'players' command?
                                        break;
                                    }
                                    break;
                            // Toggles whether we're adding new players to whitelist
                            case "addnewplayers":
                                switch (option)
                                {
                                    case "on":
                                        toggleAddNewPlayers(true, true);
                                        break;
                                    case "off":
                                        toggleAddNewPlayers(false, true);
                                        break;
                                }
                                break;
                            case "checkusernames":
                                switch (option)
                                {
                                    case "on":
                                        toggleMustMatchUsername(true, true);
                                        break;
                                    case "off":
                                        toggleMustMatchUsername(false, true);
                                        break;
                                }
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    // do nothing
                }

            }
            
        }

        /* BATTLEYE FUNCTIONS */
        
        // Sends an empty packet to keep the connection alive
        private void sendKeepAlivePacket(object sender, EventArgs e)
        {
            b.SendCommandPacket("players");
            Debug.WriteLine("Players sent");
        }

        /* WHITELIST FUNCTIONS */

        private static bool VerifyWhiteList(DayzClient client)
        {
            bool returnVal = false;

            string connStr = string.Format("server={0};user={1};database={2};port={3};password={4};", dbHost, dbUser, dbDatabase, dbPort, dbPassword);

            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = new MySqlCommand();
            MySqlDataReader rdr = null;

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "proc_CheckWhiteList";
                cmd.Parameters.Add(new MySqlParameter("p_guid", client.GUID));

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows == true)
                {
                    returnVal = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                rdr.Close();
                conn.Close();
                rdr = null;
                conn = null;
                cmd = null;
            }
            return returnVal;
        }

        private static bool VerifyWhitelistWithName(DayzClient client)
        {
            bool returnVal = false;

            string connStr = string.Format("server={0};user={1};database={2};port={3};password={4};", dbHost, dbUser, dbDatabase, dbPort, dbPassword);

            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = new MySqlCommand();
            MySqlDataReader rdr = null;

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "proc_CheckWhiteList_MatchName";
                cmd.Parameters.Add(new MySqlParameter("p_guid", client.GUID));
                cmd.Parameters.Add(new MySqlParameter("p_name", client.UserName));
                
                rdr = cmd.ExecuteReader();

                if (rdr.HasRows == true)
                {
                    returnVal = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                rdr.Close();
                conn.Close();
                rdr = null;
                conn = null;
                cmd = null;
            }
            return returnVal;
        }

        private void WelcomeMessage(DayzClient client)
        {
            if (showWelcomeMsg == true)
            {
                String msg = welcomeMessage.Replace("#name", client.UserName);
                b.SendCommandPacket(EBattlEyeCommand.Say, string.Format("-1 {0}", msg));
            }

            this.rtbDisplay.AppendText(string.Format(" Verified Player {0}: {1} - {2}\n", client.playerNo.ToString(), client.GUID.ToString(), client.UserName.ToString()));
        }

        private void KickPlayer(DayzClient client)
        {
            if (whiteListEnabled == true)
            {
                b.SendCommandPacket(EBattlEyeCommand.Kick, string.Format(@"{0} {1}", client.playerNo.ToString(), kickMessage));
                this.rtbDisplay.AppendText(string.Format(" Kicked Player {0} : {1} - {2}\n", client.playerNo.ToString(), client.GUID.ToString(), client.UserName.ToString()));
            }
            else if (addNewPlayersWhenDisabled == true)
            {
                addPlayer(client);

                if (showWelcomeMsg == true)
                {
                    String msg = welcomeMessage.Replace("#name", client.UserName);
                    b.SendCommandPacket(EBattlEyeCommand.Say, string.Format("-1 {0} [NEW PLAYER]", msg));
                }
            }
        }

        private void KickPlayerForWrongUsername(DayzClient client)
        {
            String origUsername = getDBUsername(client);
            String msg = kickMessageWrongUsername.Replace("#name", origUsername);
            b.SendCommandPacket(EBattlEyeCommand.Kick, string.Format(@"{0} {1}", client.playerNo.ToString(), msg));
            this.rtbDisplay.AppendText(string.Format(" Kicked Player {0} : {1} - {2}\n", client.playerNo.ToString(), client.GUID.ToString(), client.UserName.ToString()));
        }

        private String getDBUsername(DayzClient client)
        {
            String Username;

            string connStr = string.Format("server={0};user={1};database={2};port={3};password={4};", dbHost, dbUser, dbDatabase, dbPort, dbPassword);

            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlDataReader rdr = null;

            String query = "SELECT `name` FROM `whitelist` WHERE `identifier` = '"+client.GUID+"' LIMIT 1";

            MySqlCommand cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows == true)
                {
                    while (rdr.Read())
                    {
                        Username = rdr.GetString(0);
                        return Username;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                rdr.Close();
                conn.Close();
                rdr = null;
                conn = null;
                cmd = null;
            }
            return "";
        }

        public void addPlayer(DayzClient client)
        {
            // call insert to log function
            string connStr = string.Format("server={0};user={1};database={2};port={3};password={4};", dbHost, dbUser, dbDatabase, dbPort, dbPassword);

            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = new MySqlCommand();

            string queryString = string.Format("call proc_CheckWhiteList('{0}')", client.GUID);

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "proc_AddWhiteListed";

                cmd.Parameters.Add(new MySqlParameter("p_name", client.UserName));
                cmd.Parameters.Add(new MySqlParameter("p_email", client.email));
                cmd.Parameters.Add(new MySqlParameter("p_GUID", client.GUID));

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                this.rtbDisplay.AppendText(ex.ToString());
            }
            finally
            {
                conn.Close();
                conn = null;
                cmd = null;
            }
            this.rtbDisplay.AppendText(string.Format(" Added Player to whitelist {0} : {1} - {2}\n", client.playerNo.ToString(), client.GUID.ToString(), client.UserName.ToString()));
        }

        public void removePlayer(DayzClient client)
        {
            // call insert to log function
            string connStr = string.Format("server={0};user={1};database={2};port={3};password={4};", dbHost, dbUser, dbDatabase, dbPort, dbPassword);

            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = new MySqlCommand();

            string queryString = string.Format("call proc_CheckWhiteList('{0}')", client.GUID);

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "proc_SetWhitelistedStatus";

                cmd.Parameters.Add(new MySqlParameter("p_GUID", client.GUID)); 
                cmd.Parameters.Add(new MySqlParameter("p_whitelisted", 0));

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                this.rtbDisplay.AppendText(ex.ToString());
            }
            finally
            {
                conn.Close();
                conn = null;
                cmd = null;
            }
            this.rtbDisplay.AppendText(string.Format(" Removed Player from whitelist - {0}\n", client.GUID.ToString()));
        }

        private void LogPlayer(DayzClient client)
        {
            // call insert to log function
            string connStr = string.Format("server={0};user={1};database={2};port={3};password={4};", dbHost, dbUser, dbDatabase, dbPort, dbPassword);

            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = new MySqlCommand();

            string queryString = string.Format("call proc_CheckWhiteList('{0}')", client.GUID);

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "proc_LogWhiteList";

                cmd.Parameters.Add(new MySqlParameter("p_name", client.UserName));
                cmd.Parameters.Add(new MySqlParameter("p_GUID", client.GUID));
                cmd.Parameters.Add(new MySqlParameter("p_logtype", Convert.ToInt32(client.logType)));

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                this.rtbDisplay.AppendText(ex.ToString());
            }
            finally
            {
                conn.Close();
                conn = null;
                cmd = null;
            }
        }

        /* MISC FUNCTIONS */

        // updates 'bgcolor' depending on type of text being recieved
        private void setTextColor(String msg)
        {
            String text = msg.Substring(0, 5);

            switch (text)
            {
                case "(Side":
                    bgcolor = Color.Cyan;
                    break;
                case "RCon ":
                    bgcolor = Color.Red;
                    break;
                case "(Dire":
                    bgcolor = Color.White;
                    break;
                case "(Vehi":
                    bgcolor = Color.Yellow;
                    break;
                default:
                    bgcolor = Color.Gray;
                    break;
            }
        }
        // Turning whitelist On/Off
        private void mnuWhitelistActiveChange_Click(object sender, EventArgs e)
        {
            if (whiteListEnabled == false)
            {
                toggleWhitelist(true, false);
            }
            else
            {
                toggleWhitelist(false, false);
            }
        }
        private void toggleWhitelist(bool status, bool displayGlobally)
        {
            if (status == true)
            {
                whiteListEnabled = true;
                mnuWhitelistActiveChange.Text = "Turn Whitelist Off";
                this.rtbDisplay.AppendText("Whitelist turned on.", Color.White);
                if (displayGlobally == true)
                {
                    b.SendCommandPacket(EBattlEyeCommand.Say, "-1 WHITELIST: Whitelister turned on");
                }
            }
            else
            {
                whiteListEnabled = false;
                mnuWhitelistActiveChange.Text = "Turn Whitelist On";
                this.rtbDisplay.AppendText("Whitelist turned off.", Color.White);
                if (displayGlobally == true)
                {
                    b.SendCommandPacket(EBattlEyeCommand.Say, "-1 WHITELIST: Whitelister turned off");
                }
            }
        }
        
        // Turning welcome message On/Off
        private void mnuWelcomeChange_Click(object sender, EventArgs e)
        {
            if (showWelcomeMsg == false)
            {
                showWelcomeMsg = true;
                mnuWelcomeChange.Text = "Turn Welcome Off";
                this.rtbDisplay.AppendText("Welcome turned on.", Color.White);
            }
            else
            {
                showWelcomeMsg = false;
                mnuWelcomeChange.Text = "Turn Welcome On";
                this.rtbDisplay.AppendText("Welcome turned off.", Color.White);
            }
        }
        // Turning chat display On/Off
        private void mnuChatChange_Click(object sender, EventArgs e)
        {
            if (showChat == false)
            {
                showChat = true;
                mnuChatChange.Text = "Turn Chat Off";
                this.rtbDisplay.AppendText("Chat turned on.", Color.White);
            }
            else
            {
                showChat = false;
                mnuChatChange.Text = "Turn Chat On";
                this.rtbDisplay.AppendText("Chat turned off.", Color.White);
            }
        }
        // Whether or not to add new players to whitelist when the whitelist is disabled
        private void mnuAddNewPlayersChange_Click(object sender, EventArgs e)
        {
            if (addNewPlayersWhenDisabled == false)
            {
                toggleAddNewPlayers(true, false);
            }
            else
            {
                toggleAddNewPlayers(false, false);
            }
        }
        private void toggleAddNewPlayers(bool status, bool displayGlobally)
        {
            if (status == true)
            {
                addNewPlayersWhenDisabled = true;
                mnuAddNewPlayersChange.Text = "Don't add new players when whitelist is disabled";
                this.rtbDisplay.AppendText("New players will be automatically added to whitelist.", Color.White);
                if (displayGlobally == true)
                {
                    b.SendCommandPacket(EBattlEyeCommand.Say, "-1 WHITELIST: New players will be automatically added to whitelist");
                }
            }
            else
            {
                addNewPlayersWhenDisabled = false;
                mnuAddNewPlayersChange.Text = "Add new players when whitelist is disabled";
                this.rtbDisplay.AppendText("New players will NOT be automatically added to whitelist.", Color.White);
                if (displayGlobally == true)
                {
                    b.SendCommandPacket(EBattlEyeCommand.Say, "-1 WHITELIST: New players will NOT be automatically added to whitelist");
                }
            }
        }

        private int checkDynamicWhitelist(String msg)
        {
            String[] lines = msg.Split(new string[] { "\n" }, StringSplitOptions.None);
            String[] players = new String[lines.Length - 3];

            int count = 0;

            List<String> list = new List<String>();

            for (int i = 3; i <= lines.Length - 2; i++)
            {
                count++;
            }

            return count;

        }

        /* Loading of other forms */

        // Show settings
        private void mnuShowSettings(object sender, EventArgs e)
        {
            frmSettings frmSettings = new frmSettings(this);
            frmSettings.ShowDialog(this);
        }
        // Shows "Add GUID" form
        private void mnuAddGUID_Click(object sender, EventArgs e)
        {
            frmAddGUID frmAdd = new frmAddGUID(this);
            frmAdd.ShowDialog(this);
        }
        // Shows "Remove GUID" form
        private void mnuRemoveGUID_Click(object sender, EventArgs e)
        {
            frmRemoveGUID frmRemove = new frmRemoveGUID(this);
            frmRemove.ShowDialog(this);
        }

        private void mnuConnect_Click(object sender, EventArgs e)
        {
            tryConnect();
        }

        private void tryConnect()
        {
            
            // if we're not connected yet
            if (isConnected == false)
            {
                BattlEyeLoginCredentials logcred = new BattlEyeLoginCredentials { Host = ip, Password = password, Port = Convert.ToInt32(port) };
                b = new BattlEyeClient(logcred);

                keepAliveTimer = new System.Timers.Timer();
                keepAliveTimer.Elapsed += new ElapsedEventHandler(sendKeepAlivePacket);
                keepAliveTimer.Interval = 10000; // in miliseconds
                keepAliveTimer.Start();

                rtbDisplay.AppendText("\n Connecting...\n");

                // make the connection
                b.MessageReceivedEvent += DumpMessage;
                b.DisconnectEvent += Disconnected;
                //b.ReconnectOnPacketLoss(true);
                b.Connect();

                if (b.IsConnected() == true)
                {
                    isConnected = true;
                    mnuConnect.Enabled = false;
                    //b.SendCommandPacket("");

                    //KeepAlivePacket k = new KeepAlivePacket(b);
                    //Thread oThread = new Thread(new ThreadStart(k.SendKeepAlivePacket));
                    //oThread.Start();
                    //while (!oThread.IsAlive) ;

                    if (showStartupMsg == true)
                    {
                        b.SendCommandPacket(EBattlEyeCommand.Say, "-1 WHITELIST: Started");
                    }
                }
            }
        }

        private void startReconnect()
        {
            reconnectTimer = new System.Windows.Forms.Timer();
            reconnectTimer.Tick += new EventHandler(tryReconnect);
            reconnectTimer.Interval = 60000; // in miliseconds (30 seconds)
            reconnectTimer.Start();
        }

        private void tryReconnect(object sender, EventArgs e)
        {
            Thread.Sleep(5);
            // if we're not connected, we'll try to connect
            if (b.IsConnected() == false)
            {
                tryConnect();
            }
            // otherwise we can kill the timer
            else
            {
                reconnectTimer = null;
                keepAliveTimer.Start();
            }
        }

        public void enableConnectButton()
        {
            this.mnuConnect.Enabled = true;
        }

        public void updateSettings()
        {
            Properties.Settings.Default.Upgrade(); // Needed?

            // Rcon
            ip = Properties.Settings.Default.rconIP;
            port = Properties.Settings.Default.rconPort;
            password = Properties.Settings.Default.rconPass;
            // Database
            dbHost = Properties.Settings.Default.dbHost;
            dbPort = Properties.Settings.Default.dbPort.ToString();
            dbPassword = Properties.Settings.Default.dbPass;
            dbDatabase = Properties.Settings.Default.dbDatabase;
            dbUser = Properties.Settings.Default.dbUser;
            // Misc
            welcomeMessage = Properties.Settings.Default.welcomeMessage;
            kickMessage = Properties.Settings.Default.kickMessage;
            dynamicWhitelistEnabled = Properties.Settings.Default.dynamicWhitelistEnabled;

            enableWhitelistBelow = Properties.Settings.Default.enableWhitelistBelow;
            disableWhitelistAbove = Properties.Settings.Default.disableWhitelistAbove;
        }

        public void kickNonWhitelistPlayers(String playerList)
        {
            String[] lines = playerList.Split(new string[] { "\n" }, StringSplitOptions.None);
            Match matchString;

            foreach(String line in lines)
            {
                // this regex is piss-poor. // (?<guid>[0-9a-f]{32})
                matchString = Regex.Match(line, @"(?<player_id>[0-9]{1,3})\s+(?<ip>.+):(?<port>[0-9]{1,5})\s+(?<ping>[0-9]+)\s+(?<guid>.+)\(OK\)(?<name>.+)", RegexOptions.IgnoreCase);
                if (matchString.Success)
                {
                    DayzClient client = new DayzClient();

                    String guid = matchString.Groups["guid"].Value.Trim();
                    //String[] foo = guid.Split(' ');
                    String[] foo = Regex.Split(guid, @"\s+");
                    client.GUID = foo[1];
                    client.playerNo = Convert.ToInt32(matchString.Groups["player_id"].Value);
                    client.UserName = matchString.Groups["user"].Value;

                    if (VerifyWhiteList(client) == false)
                    {
                        b.SendCommandPacket(EBattlEyeCommand.Kick, string.Format(@"{0} {1}", client.playerNo.ToString(), kickMessage));
                    }
                }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout aboutForm = new frmAbout();
            aboutForm.ShowDialog();
        }

        private void mnuCheckUsernamesMatch_Click(object sender, EventArgs e)
        {
            if (mustMatchUsername == false)
            {
                toggleMustMatchUsername(true, false);
            }
            else
            {
                toggleMustMatchUsername(false, false);
            }
        }

        private void toggleMustMatchUsername(bool status, bool displayGlobally)
        {
            if (status == true)
            {
                mustMatchUsername = true;
                mnuAddNewPlayersChange.Text = "Don't check usernames match database";
                this.rtbDisplay.AppendText("Username check has been enabled.", Color.White);
                if (displayGlobally == true)
                {
                    b.SendCommandPacket(EBattlEyeCommand.Say, "-1 WHITELIST: Username check enabled");
                }
            }
            else
            {
                mustMatchUsername = false;
                mnuAddNewPlayersChange.Text = "Check usernames match database";
                this.rtbDisplay.AppendText("Username check has been disabled.", Color.White);
                if (displayGlobally == true)
                {
                    b.SendCommandPacket(EBattlEyeCommand.Say, "-1 WHITELIST: Username check disabled");
                }
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            b.SendCommandPacket("players");
        }
    }

    public class DayzClient
    {
        public string GUID { get; set; }
        public string IP { get; set; }
        public string UserName { get; set; }
        public string message { get; set; }
        public string email { get; set; }
        public int playerNo { get; set; }

        public LogTypes logType { get; set; }

        public enum LogTypes
        {
            Success = 1,
            Kick = 2
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(" " + text + "\n");
            box.SelectionColor = box.ForeColor;
            box.SelectionStart = box.Text.Length;
            box.ScrollToCaret();
        }
    }

    class KeepAlivePacket
    {
        private IBattleNET b;

        public KeepAlivePacket(IBattleNET b)
        {
            // TODO: Complete member initialization
            this.b = b;
        }

        public void SendKeepAlivePacket()
        {
            Debug.WriteLine("Send Keep Alive Packet");
            this.b.SendCommandPacket("players");
            Thread.Sleep(30000);
        }

    }

}
